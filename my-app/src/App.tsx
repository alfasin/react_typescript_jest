import * as React from 'react';
import './App.css';
import logo from './logo.svg'

class App extends React.Component {

    public label = 'Hello React';
    public handleClick: ((event: React.MouseEvent<HTMLHeadingElement>) => void) | undefined;

    constructor () {
        // @ts-ignore
        super()
        this.handleClick = () => alert(this.label)
    }

    public render() {
        const that = this
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React!</h1>
                    <h5 onClick={that.handleClick}>{this.label}</h5>

                </header>
                <p className="App-intro">
                    To get started, edit <code>src/App.tsx</code> and save to reload.
                </p>
            </div>
        );

    }
}

export default App;