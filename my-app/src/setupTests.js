"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Enzyme = require("enzyme");
var Adapter = require("enzyme-adapter-react-16");
Enzyme.configure({
    adapter: new Adapter(),
});
