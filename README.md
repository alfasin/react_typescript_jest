# React+TypeScript+Jest Tutorial Material

This repo contains sources and documentation for a one-hour 
tutorial on learning React+TypeScript, with Jest.

docs: https://react-typescript-tdd.firebaseapp.com/index.html#

repo forked from: https://github.com/pauleveritt/react_typescript_jest
